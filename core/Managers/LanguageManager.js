const Bot = require('../InfinityBot');
const path = require('path');

const LANG_DIR = 'lang';
const FALLBACK_LANG = Bot.config.FALLBACK_LANG;

let GuildLanguageManagers = {};
class LanguageManager
{
    constructor(guild, lang)
    {
        this.guild = guild;
        this.setLang(lang);
    }

    getLangId() { return this.langId; }

    setLang(lang) 
    {
        this.lang = LanguageManager.doesLanguageExists(lang);
        this.langId = lang;

        this.updateDB(this.guild, lang);
    }

    async updateDB(guild, lang)
    {
        Bot.db.guilds.findAndModify({ query: { guildId: guild }, update: { $set: { langId: lang } } }, (err, d, le) =>
        {
            if (err)
                Bot.Logger.error(err);

            if (!d)
                Bot.db.guilds.insert({ guildId: guild, langId: lang });
        });
    }

    get(key, ...replace)
    {
        if (!this.lang)
            return undefined;

        let result = this.lang;
        let keySplit = key.split('.');

        keySplit.map(key => 
        {
            if (result[key])
                result = result[key];
        });

        if (typeof result !== 'string')
            return key;

        replace.map(r => result = result.replace(/(::)[A-z0-9]+/, r));
        return result;
    }

    static getLanguageManager(guild, lang)
    {
        if (!guild)
            return;

        let GuildLanguageManager = GuildLanguageManagers[guild];
        if (!lang && guild)
        {
            if (GuildLanguageManager)
                return GuildLanguageManager;
            
            return LanguageManager.getLanguageManager(guild, FALLBACK_LANG);
        }

        if (GuildLanguageManager)
        {
            GuildLanguageManager.setLang(lang);
            return GuildLanguageManager;
        }

        GuildLanguageManagers[guild] = new LanguageManager(guild, lang);
        return GuildLanguageManagers[guild];
    }

    static doesLanguageExists(lang)
    {
        try 
        {
            lang = require(path.resolve(__dirname, '../..', LANG_DIR, lang));
        }
        catch (err)
        {
            return false;
        }

        return lang;
    }
}

module.exports = LanguageManager;