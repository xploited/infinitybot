const Bot = require('../InfinityBot');

class EmbedManager
{
    static newEmbed()
    {
        return new Bot.Discord.RichEmbed().setColor(Bot.config.EMBED_COLOR);
    }

    static newErrorEmbed()
    {
        return new Bot.Discord.RichEmbed();
    }
}

module.exports = EmbedManager;