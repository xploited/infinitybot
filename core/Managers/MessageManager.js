const Bot = require('../InfinityBot');
class MessageManager
{
  constructor(Client, CommandManager)
  {
    Client.on('message', message => this.handleMessage(message));

    this.Client = Client;
    this.CommandManager = CommandManager;
  }

  handleMessage(message)
  {
    if (message.author.id === this.Client.user.id && message.channel.type === 'text')
      return message.delete(Bot.config.delays.BOT_MESSAGE_DELETE_DELAY * 1000).catch(err => Bot.Logger.warn(err));

    if (this.CommandManager.parseCommand(this.Client, message) && message.channel.type === 'text')
      return message.delete(Bot.config.delays.USER_COMMAND_DELETE_DELAY * 1000).catch(err => Bot.Logger.warn(err));
  }
}

module.exports = MessageManager;
