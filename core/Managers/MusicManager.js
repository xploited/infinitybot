const Bot = require('../InfinityBot');
const ytdl = require('ytdl-core');
const EmbedManager = require('./EmbedManager');
const LanguageManager = require('./LanguageManager');

let MusicManagers = {};
class MusicManager
{
    constructor(client, guild)
    {
        this.client = client;
        this.guild = guild;
        this.lang = LanguageManager.getLanguageManager(guild.id);

        this.songQueue = [];
        this.sleepingQueue = [];
        this.currentlyPlaying = undefined;

        MusicManagers[guild.id] = this;
    }

    queueSong(query, voiceChannel, textChannel, cursor = 0)
    {
        if (!voiceChannel)
        {
            let embed = EmbedManager.newErrorEmbed()
                .setTitle(this.lang.get('music.not_in_voice.title'))
                .setDescription(this.lang.get('music.not_in_voice.description'));

            return textChannel.send({ embed });
        }

        if (!query)
            return MusicManager._queryNotValid(textChannel, this.lang);

        if (query.match(/(https:\/\/open.spotify.com\/user\/)[A-z0-9]+(\/playlist\/)[A-z0-9]+/))
        {
            let split = query.split('/user/')[1].split('/playlist/');
            let user = split[0];
            let playlist = split[1];

            return this._requestSpotify(query, voiceChannel, textChannel, user, playlist, cursor);
        } 
        else if (query.match(/(spotify\:user\:)[A-z0-9]+(\:playlist\:)[A-z0-9]+/))
        {
            let split = query.split(':user:')[1].split(':playlist:');
            let user = split[0];
            let playlist = split[1];

            return this._requestSpotify(query, voiceChannel, textChannel, user, playlist, cursor);
        }
        else if (cursor >= 50)
        {
            return this._removeFromSleepingQueue(query);
        }

        if (!ytdl.validateID(query) && !ytdl.validateURL(query))
        {
            Bot.Youtube.search.list({
                q: query,
                maxResults: 1,
                part: 'snippet'
            }, 
            (err, data) => 
            {
                if (err)
                    return MusicManager._sendUserError(textChannel, err, this.lang);

                if (data.items.length > 0)
                {
                    data = data.items[0];
                    if (data.id.kind === 'youtube#playlist')
                    {
                        Bot.Youtube.playlistItems.list({
                            playlistId: data.id.playlistId,
                            maxResults: cursor + 1,
                            part: 'snippet'
                        }, 
                        (err, pd) => 
                        {
                            if (err)
                                return MusicManager._sendUserError(textChannel, err, this.lang);

                            if (pd.items.length > cursor)
                            {
                                this.queueSong(pd.items[cursor].snippet.resourceId.videoId, voiceChannel, textChannel);
                                this._addToSleepingQueue(query, voiceChannel, textChannel, cursor + 1);
                            }
                            else if (cursor > 0)
                            {
                                this._removeFromSleepingQueue(query);
                            }
                            else
                            {
                                MusicManager._queryNotValid(textChannel, this.lang);
                            }
                        });
                    }
                    else
                    {
                        this.queueSong(data.id.videoId, voiceChannel, textChannel);
                    }
                }
                else
                {
                    MusicManager._queryNotValid(textChannel, this.lang);
                }
            });

            return;
        }

        let id = ytdl.getVideoID(query);
        if (id instanceof Error || !id)
            return MusicManager._queryNotValid(textChannel, this.lang);

        let url = 'https://www.youtube.com/watch?v=' + id;
        ytdl.getInfo(url, (err, info) => 
        {
            if (err)
                return MusicManager._sendUserError(textChannel, err, this.lang);

            this.songQueue.push({ videoUrl: url, videoInfo: info, voiceChannel: voiceChannel, textChannel: textChannel });
            if (!this.currentlyPlaying)
                return this.nextSong();
                
            let embed = MusicManager._musicEmbed(info, this.lang.get('music.queue.add', this.songQueue.length), info.title);
            textChannel.send({ embed });
        });
    }

    nextSong()
    {
        if (this.sleepingQueue.length > 0 && this.songQueue.length === 0)
        {
            this.end();
            let sleepingSong = this.sleepingQueue.shift();
            return this.queueSong(sleepingSong.query, sleepingSong.voiceChannel, sleepingSong.textChannel, sleepingSong.cursor);
        }
        else if (this.songQueue.length === 0)
        {
            return this.end();
        }

        let song = this.songQueue.shift();
        this.currentlyPlaying = song;
        song.voiceChannel.join().then(connection => 
        {
            const stream = ytdl(song.videoUrl, { filter: 'audioonly' });
            const dispatcher = connection.playStream(stream);

            let embed = MusicManager._musicEmbed(song.videoInfo, this.lang.get('music.playing_now'), song.videoInfo.title);
            song.textChannel.send({ embed });

            dispatcher.once('end', () => setTimeout(() => this.nextSong(), 200)); // timeout is necessary since it doesnt work on Ubuntu without it

            this.currentlyPlaying['dispatcher'] = dispatcher;
            this.currentlyPlaying['connection'] = connection;

        }).catch(err => Bot.Logger.error(err));
    }

    skipSong(textChannel)
    {
        if (!this.currentlyPlaying || !this.currentlyPlaying.dispatcher)
            return MusicManager._sendUserError(textChannel, this.lang.get('music.error_ending'), this.lang);

        if (this.songQueue.length === 0 && this.sleepingQueue.length === 0)
            return MusicManager._sendUserError(textChannel, this.lang.get('music.error_skipping'), this.lang);

        this.currentlyPlaying.dispatcher.end();
    }

    end(textChannel, now)
    {
        let endNow = (textChannel, connection) => 
        {
            this.songQueue = [];
            this.sleepingQueue = [];
            let embed = EmbedManager.newEmbed()
                .setTitle(this.lang.get('music.ended.title'))
                .setDescription(this.lang.get('music.ended.description'));

            textChannel.send({ embed });

            if (connection)
            {
                connection.disconnect();
            }
            else
            {
                let conn = this.client.voiceConnections.array().filter(connection => connection.channel.guild === this.guild);
                conn.map(connection => connection.disconnect());
            }
        }

        if (now && this.currentlyPlaying)
        {
            textChannel = this.currentlyPlaying.textChannel;
            let connection = this.currentlyPlaying.connection;
            this.currentlyPlaying = undefined;
            endNow(textChannel, connection);
        }
        else if (textChannel && now && !this.currentlyPlaying)
        {
            MusicManager._sendUserError(textChannel, this.lang.get('music.error_ending'), this.lang);
        }
        else if (!now && this.currentlyPlaying)
        {
            textChannel = this.currentlyPlaying.textChannel;
            let connection = this.currentlyPlaying.connection;
            this.currentlyPlaying = undefined;
            this.client.setTimeout(() => 
            {
                if (!this.currentlyPlaying)
                    endNow(textChannel, connection);
            }, 5000);
        }
    }

    getQueue(textChannel)
    {
        let embed = EmbedManager.newEmbed()
            .setTitle(this.lang.get('music.queue.queue'));

        if (this.songQueue.length > 0)
            this.songQueue.map((song, i) => embed.addField(`#${i + 1}`, song.videoInfo.title, false));
        else 
            embed.setDescription(this.lang.get('music.queue.empty'));

        textChannel.send({ embed });
    }

    _requestSpotify(originalQuery, voiceChannel, textChannel, userId, playlistId, cursor)
    {
        Bot.spotifyApi.getPlaylist(userId, playlistId, { fields: 'tracks.total' }, (err, data) => 
        {
            if (err)
                return MusicManager._sendUserError(textChannel, err, this.lang);

            if (data.body.tracks.total >= (cursor + 1))
            {
                let limit = 1;
                Bot.spotifyApi.getPlaylistTracks(userId, playlistId, { limit, offset: cursor }, (err, data) =>
                {
                    if (err)
                        return MusicManager._sendUserError(textChannel, err, this.lang);
        
                    let items = data.body.items;
                    if (!items)
                    {
                        this._removeFromSleepingQueue(originalQuery);
                        return MusicManager._queryNotValid(textChannel, this.lang);
                    }

                    items.map(item => 
                    {
                        let query = `${item.track.album.artists[0].name} - ${item.track.name}`;
                        this.queueSong(query, voiceChannel, textChannel, cursor);
                    });
                });
        
                this._addToSleepingQueue(originalQuery, voiceChannel, textChannel, cursor + limit);
            }
            
            if (data.body.tracks.total === (cursor + 1))
                this._removeFromSleepingQueue(originalQuery);
        });
    }

    _addToSleepingQueue(query, voiceChannel, textChannel, cursor)
    {
        let added = false;
        this.sleepingQueue.map(item => 
        {
            if (item.query === query)
            {
                item.voiceChannel = voiceChannel;
                item.textChannel = textChannel;
                item.cursor = cursor;
                added = true;
            }
        });

        if (!added)
            this.sleepingQueue.push({ query, voiceChannel, textChannel, cursor });
    }

    _removeFromSleepingQueue(query)
    {
        this.sleepingQueue.map((item, i) => 
        {
            if (item.query === query)
                this.sleepingQueue.splice(i, 1);
        });
    }

    static getMusicManager(client, guild)
    {
        let manager =  MusicManagers[guild.id];
        if (manager)
            return manager;

        return new MusicManager(client, guild);
    }

    static _musicEmbed(info, title, description)
    {
        return new Bot.Discord.RichEmbed()
            .setAuthor(info.author.name, info.author.avatar, info.author.user_url)
            .setTitle(title)
            .setDescription(description)
            .setThumbnail(info.iurlhq)
            .setColor(Bot.config.EMBED_COLOR);
    }

    static _queryNotValid(textChannel, lang)
    {
        let embed = EmbedManager.newErrorEmbed()
            .setTitle(lang.get('music.error'))
            .setDescription(lang.get('music.query_not_valid'));

        return textChannel.send({ embed });
    }

    static _sendUserError(textChannel, err, lang)
    {
        let embed = EmbedManager.newErrorEmbed()
            .setTitle(lang.get('music.error'))
            .setDescription(err);

        return textChannel.send({ embed });
    }
}

module.exports = MusicManager;