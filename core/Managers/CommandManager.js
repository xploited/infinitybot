const Bot = require('../InfinityBot');
const fs = require('fs');
const path = require('path');
const LanguageManager = require('./LanguageManager');
const EmbedManager = require('./EmbedManager');

const COMMANDS_DIR = 'Commands';

let commands = {};
class CommandManager
{
  static registerCommand(command, commandEx) { commands[command] = commandEx; }

  static loadCommands()
  {
    return new Promise((resolve, reject) =>
    {
      fs.readdir(path.resolve('./core/', COMMANDS_DIR), (err, files) =>
      {
        if (err)
          return reject(err);

        files.forEach(file =>
        {
          let commandEx = require(path.resolve(__dirname, '..', COMMANDS_DIR, file));
          let fSplit = file.split('_');

          if (fSplit.length < 2 || fSplit[0] != 'c')
            return reject(`Couldn't split the command name ${file}`);

          let command = fSplit[1].split('.')[0]; // c_play.js
          CommandManager.registerCommand(command, commandEx);

        });

        return resolve();
      });
    });
  }

  static parseCommand(client, message)
  {
    let content = message.content;
    if (!content.startsWith(Bot.config.prefix))
    {
      if (content.startsWith(Bot.config.sudoPrefix))
      {
        Bot.db.superusers.find({ userId: message.author.id }).toArray((err, data) => 
        {
          if (err)
            return Bot.Logger.error(err);

          if (data.length === 0)
            return;

          const args = content.slice(Bot.config.sudoPrefix.length).trim().split(' ');
          const command = args.shift().toLowerCase();
          
          CommandManager._sudoCommands(client, message, command, args.join(' '));
        });

        return true;
      }

      return false;
    }

    const channel = message.channel;
    if (channel.type !== 'text')
    {
      let defLang = LanguageManager.getLanguageManager("0");
      let embed = EmbedManager.newErrorEmbed()
        .setTitle(defLang.get('not_in_guild.title'))
        .setDescription(defLang.get('not_in_guild.description'));

      return channel.send({ embed });
    }

    const guild = message.guild;
    const args = content.slice(Bot.config.prefix.length).trim().split(' ');
    const command = args.shift().toLowerCase();

    let commandEx = commands[command];
    if (!commandEx)
    {
      let lang = LanguageManager.getLanguageManager(guild.id);
      let embed = EmbedManager.newErrorEmbed()
        .setTitle(lang.get('not_found.title'))
        .setDescription(lang.get('not_found.description'));

      channel.send({ embed });
      return true;
    }

    if (typeof commandEx.execute !== 'function')
    {
      Bot.Logger.error('Error processing the command!');
      return true;
    }

    commandEx.execute(client, message, args);
    return true;
  }

  static notEnoughArguments(channel, needed)
  {
    const lang = LanguageManager.getLanguageManager(channel.guild.id);
    const embed = EmbedManager.newErrorEmbed()
      .setTitle(lang.get('not_enough_arguments.title'))
      .setDescription(lang.get('not_enough_arguments.description', needed));
    
    channel.send({ embed });
  }

  static _sudoCommands(client, message, command, args)
  {
    let channel = message.channel;
    if (command === 'sudo' && args === 'login')
    {
      console.log('login');
    }
    else if (command === 'sudo' && args === 'logout')
    {
      console.log('logout');
    }
    else if (command === 'eval')
    {
      channel.send(`\`\`\`${eval(args)}\`\`\``);
    }
  }
}

module.exports = CommandManager;
