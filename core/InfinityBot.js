// InfinityBot by iamkryfor
const MongoClient = require('mongojs');
const Youtube = require('youtube-api');
const SpotifyWebApi = require('spotify-web-api-node');
const winston = require('winston');
const Discord = require('discord.js');
const config = require('../config.json');
const Client = new Discord.Client();

const Logger = winston.createLogger({
  level: 'info',
  format: winston.format.simple(),
  transports: [
    new winston.transports.File({ filename: 'combined.log' }),
    new winston.transports.Console()
  ]
});

const mongoUrl = `mongodb://${config.MONGODB.user}:${config.MONGODB.password}@${config.MONGODB.host}/${config.MONGODB.db}?authSource=admin`;
let db = MongoClient(mongoUrl);

Youtube.authenticate({
  type: 'key',
  key: config.YOUTUBE_KEY
});

let spotifyApi = new SpotifyWebApi({
  clientId : config.SPOTIFY.clientId,
  clientSecret : config.SPOTIFY.clientSecret,
});

function getAccessToken()
{
  spotifyApi.clientCredentialsGrant().then(data =>
  {
    let access_token = data.body.access_token;
    let expires_in = parseInt(data.body.expires_in) * 1000;
    Logger.info(`New Spotify Access Token ${access_token} expires in ${data.body.expires_in/60} minutes`);

    spotifyApi.setAccessToken(access_token);
    Client.setInterval(() => getAccessToken(), expires_in);
  })
  .catch(err => 
  {
    Logger.error(err);
    process.exit(10);
  });
}

getAccessToken();

db.on('error', (err) => 
{
  Logger.error(err);
  process.exit(8);
});

module.exports = { Discord, db, Youtube, spotifyApi, Logger, config };

const CommandManager = require('./Managers/CommandManager');
const MessageManager = require('./Managers/MessageManager');
const LanguageManager = require('./Managers/LanguageManager');

db.guilds.find({}).toArray((err, data) => 
{
  if (err)
    Logger.error(err);

  data.map(guild => LanguageManager.getLanguageManager(guild.guildId, guild.langId));
  CommandManager.loadCommands().then(() =>
  {
    Client.on('ready', () =>
    {
      new MessageManager(Client, CommandManager);
      
      let game = config.PLAYING_MESSAGES[Math.floor(Math.random() * config.PLAYING_MESSAGES.length)];
      Logger.info(`Client Ready! Playing ${game}`);
      Client.user.setGame(game);
    });
  
    Client.login(config.DISCORD_TOKEN);
  
  }).catch(err => Logger.error(err));
});