const MusicManager = require('../Managers/MusicManager');

class c_skip
{
    static execute(client, message, args)
    {
        let channel = message.channel;
        let guild = message.guild;
        
        MusicManager.getMusicManager(client, guild)
            .skipSong(channel);
    }
}

module.exports = c_skip;