const MusicManager = require('../Managers/MusicManager');

class c_play 
{
    static execute(client, message, args)
    {
        let channel = message.channel;
        let guild = message.guild;

        MusicManager.getMusicManager(client, guild)
            .getQueue(channel);
    }
}

module.exports = c_play;