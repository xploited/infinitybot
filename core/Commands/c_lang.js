const Bot = require('../InfinityBot');
const LanguageManager = require('../Managers/LanguageManager');
const EmbedManager = require('../Managers/EmbedManager');
const CommandManager = require('../Managers/CommandManager');

class c_lang 
{
    static execute(client, message, args)
    {
        const channel = message.channel;
        const guild = message.guild;
        if (args.length < 1)
            return CommandManager.notEnoughArguments(channel, 1);

        let lang = LanguageManager.getLanguageManager(guild.id, null);
        const l = args[0];

        if (lang.getLangId() === l)
        {
            const embed = EmbedManager.newErrorEmbed()
                .setTitle(lang.get('lang.same_language.title'))
                .setDescription(lang.get('lang.same_language.description', l));

            return channel.send({ embed });
        }

        if (!LanguageManager.doesLanguageExists(l))
        {
            const embed = EmbedManager.newErrorEmbed()
                .setTitle(lang.get('lang.doesnt_exist.title'))
                .setDescription(lang.get('lang.doesnt_exist.description', l));

            return channel.send({ embed });
        }

        lang = LanguageManager.getLanguageManager(guild.id, l);
        const embed = EmbedManager.newEmbed()
            .setTitle(lang.get('lang.title'))
            .setDescription(lang.get('lang.description', message.author.username, l))
            .setColor(Bot.config.EMBED_COLOR);

        channel.send({ embed });
    }
}

module.exports = c_lang;