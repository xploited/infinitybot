const Bot = require('../InfinityBot');
const EmbedManager = require('../Managers/EmbedManager');
const LanguageManager = require('../Managers/LanguageManager');

class c_about 
{
    static execute(client, message, args)
    {
        const guild = message.guild;
        const channel = message.channel;
        const lang = LanguageManager.getLanguageManager(guild.id);
        let embed = EmbedManager.newEmbed()
            .setTitle(lang.get('about.title', client.user.username))
            .setDescription(lang.get('about.description'));

        client.fetchUser(Bot.config.IAMKRYFOR_ID).then(user => 
        {
            embed.setFooter(lang.get('about.footer', user.username), user.avatarURL);
            channel.send({ embed });
        }).catch(err => 
        {
            Bot.Logger.error(err);
            channel.send({ embed });
        });
    }
}

module.exports = c_about;