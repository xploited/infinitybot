const MusicManager = require('../Managers/MusicManager');

class c_stop
{
    static execute(client, message, args)
    {
        let channel = message.channel;
        let guild = message.guild;

        MusicManager.getMusicManager(client, guild)
            .end(channel, true);
    }
}

module.exports = c_stop;