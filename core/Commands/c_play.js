const MusicManager = require('../Managers/MusicManager');
const EmbedManager = require('../Managers/EmbedManager');

class c_play 
{
    static execute(client, message, args)
    {
        let guild = message.guild;
        let channel = message.channel;
        let voiceChannel = message.member.voiceChannel;

        MusicManager.getMusicManager(client, guild)
            .queueSong(args.join(' '), voiceChannel, channel);
    }
}

module.exports = c_play;